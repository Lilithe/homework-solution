import styles from './Search.module.css'
import Header from '../Header/Header';


const Search = () => {
    return (
        <section className={styles.searchSection}>
                <Header type="3" text="Search"  />
                <div className={styles.searchContainer}>
                    <input className={`${styles.mainSearchBar} ${styles.inputStyle}`} placeholder="Type your search" />
                    <button className={`${styles.mainSearchAllBtn} ${styles.searchBtnStyle}`}>All</button>
                    <button className={`${styles.mainSearchImportantBtn} ${styles.searchBtnStyle}`}>Important</button>
                    <button className={`${styles.mainSearchDoneBtn} ${styles.searchBtnStyle}`}>Done</button>
                </div>
        </section>
    )
}

export default Search