// import React from 'react';
// import styles from './TaskItem.module.css';
// import Button from '../Button/Button';


// class TaskItem extends React.Component {
//     state = {
//         important: false,
//         editing: false
//     }
//     onImportantClick = (event) => {
//         this.setState({
//             important: !this.state.important
//         })
//     }
//     edit = (event) => {
//         this.setState({
//             editing: !this.state.editing
//         });
//         event.preventDefault();
//         console.log(this.state.editing);
//         let editBtn = document.getElementById('editBtn');
//         let taskInput = document.getElementById('taskInput');
//         if (!this.state.editing) {
//             editBtn.innerHTML = "Save";
//             taskInput.readOnly = false;
            
//           } else {
//             editBtn.innerHTML = "Edit";
//             taskInput.readOnly = true;
            
//         }
//     }
    
//     render() {
        
//     const {title, important, edit} = this.props.task;
    
//     const styleImportant = {
//         color: this.state.important ? "red" : "black"
//     }
//     const styleEdit = {
//         readOnly: this.state.edit ? false : true,
//     }
//     if(document.getElementById('editBtn')) {
//         return (
//             <div className={styles.taskContainer}>
//                 {
//                     <div className={styles.taskMenu}>  {/* Each child in a list should have a unique "key" prop. */}
//                         <input id='taskInput' readOnly  style={{...styleImportant, ...styleEdit}}/>
//                         <button id='editBtn'  className={`${styles.taskBtn} ${styles.editBtn}`} text="Edit" onClick={this.edit}>Edit</button>
//                         {/* <Button className={`${styles.taskBtn} ${styles.editBtn}`} text="Edit" onClick={this.edit}/> */}
//                         <button className={`${styles.taskBtn} ${styles.importantBtn}`} onClick = {this.onImportantClick}>Important</button>
//                         <button className={`${styles.taskBtn} ${styles.doneBtn}`}>Done</button>
//                         <button className={`${styles.taskBtn} ${styles.deleteBtn}`}>Delete</button>
//                     </div>    
//                 }
//             </div>
//             )}
//     else { 
//         return (
//             <div className={styles.taskContainer}>
//                 {
//                     <div className={styles.taskMenu}>  {/* Each child in a list should have a unique "key" prop. */}
//                         <input id='taskInput' readOnly value = {title}  style={{...styleImportant, ...styleEdit}}/>
//                         <button id='editBtn'  className={`${styles.taskBtn} ${styles.editBtn}`} text="Edit" onClick={this.edit}>Edit</button>
//                         {/* <Button className={`${styles.taskBtn} ${styles.editBtn}`} text="Edit" onClick={this.edit}/> */}
                        
//                         <button className={`${styles.taskBtn} ${styles.doneBtn}`}>Done</button>
//                         <button className={`${styles.taskBtn} ${styles.deleteBtn}`}>Delete</button>
//                     </div>    
//                 }
//             </div>
//     )
//     }
//     }  
//     }


// export default TaskItem;
import React from 'react';
import styles from './TaskItem.module.css';


class TaskItem extends React.Component {
    constructor(props){
      super()
      this.state = {
        text: props.item,
        isEditing: false,
        important: false,
      };
      this.onClickEdit = this.onClickEdit.bind(this);
      this.onSaveEdit = this.onSaveEdit.bind(this);
      this.onTextChanged = this.onTextChanged.bind(this);
    }
    onImportantClick = (event) => {
      this.setState({
          important: !this.state.important
      })
    }
    onClickEdit(){
      this.setState({isEditing: !this.state.isEditing});
    }
    onSaveEdit(){
      this.setState({
        isEditing: false
      });
    }
    onTextChanged(e){
      this.setState({
        text: e.target.value,
      });
    }
    render(){
      const {title} = this.props.task;
      const styleImportant = {
        color: this.state.important ? "red" : "black"
    }
      return(
        <div className={styles.taskContainer}>
        <li>
        {this.state.isEditing 
          ? <span>
              <input value={this.state.text} type="text" style={styleImportant} onChange={this.onTextChanged}/>
            </span> 
          :<span>
              <input value={title} type="text" style={styleImportant} onChange={this.onTextChanged}/>
           </span> }
        {this.state.isEditing 
        ? <button className={`${styles.taskBtn} ${styles.editBtn}`} onClick={this.onSaveEdit}>Save</button>
        : <button className={`${styles.taskBtn} ${styles.editBtn}`} onClick={this.onClickEdit}>Edit</button>}
        
        <button className={`${styles.taskBtn} ${styles.importantBtn}`} onClick = {this.onImportantClick}>Important</button>
        <button className={`${styles.taskBtn} ${styles.doneBtn}`}>Done</button>
        <button className={`${styles.taskBtn} ${styles.deleteBtn}`}>Delete</button>
        </li>
        </div>
      )
    }
  }

  export default TaskItem