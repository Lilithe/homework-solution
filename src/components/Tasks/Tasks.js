import React from 'react';
import Header from '../Header/Header';
import TaskItem from '../TaskItem/TaskItem';
import styles from './Tasks.module.css';


class Tasks extends React.Component {
    state = {
        important: false
    }
    onImportantClick = (event) => {
        this.setState({
            important: !this.state.important
        })
    }
    render() {
    const taskList = [
        {id: 1, title: "Learn HTML, CSS"},
        {id: 2, title: "Learn JS"},
        {id: 3, title: "Learn React"},
        {id: 4, title: "Learn Node.js"},
    ]
   
        return (
                <section className={styles.tasksSection}>
                    <Header text="Tasks" type="3" />
                    <div className={styles.taskContainer}>
                        {
                            taskList.map(task => <TaskItem key= {task.id} task = {task} />)
                        }
                    </div>
                </section>
        )
    }
}
export default Tasks;