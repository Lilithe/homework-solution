import React from 'react';
function Button({text, styleClass}) {
    let btn = null;
    btn = React.createElement('button', {}, text);
    return (
        <>
            {btn}
        </>
    );
}
export default Button;