import styles from '../CreateTask/CreateTask.module.css'
import React from 'react';
function Header({type, text}) {
    let title = React.createElement(`h${type}`, {}, text);
    // switch (type) {
    //     case 'h1': title = <h1>{text}</h1>; break;
    //     case 'h2': title = <h2>{text}</h2>; break;        
    //     case 'h3': title = <h3>{text}</h3>; break;
    //     case 'h4': title = <h4>{text}</h4>; break;
    //     case 'h5': title = <h5>{text}</h5>; break;
    //     case 'h6': title = <h6>{text}</h6>; break;
    //     default:   title = <h6>{text}</h6>; break;
    // }
    return (
        <>
        <div className={styles.tasksTitle}>
            {title}
        </div>
        </>
    );
}
export default Header