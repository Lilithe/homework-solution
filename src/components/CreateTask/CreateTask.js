import styles from './CreateTask.module.css'
import Header from '../Header/Header';

const CreateTask = () => {
    return (
        <header>
            <Header text="My Tasks" type="2" />
            <div className={styles.headerTaskContainer}>
                <input className={`${styles.headerTaskName} ${styles.inputStyle}`} placeholder='What do you have planned?'/>
                <button className={styles.headerAddTaskBtn}>Add Task</button>
            </div>
        </header>
    )
 };
  
 export default CreateTask;