import CreateTask from "../CreateTask/CreateTask";
import Tasks from "../Tasks/Tasks";
import Search from "../Search/Search";
import './App.css';
 
const App = () => {
 return (
    <>
        <CreateTask />
        <main>
            <Search />
            <Tasks />
        </main>
    </>
 );
}

export default App;
 
